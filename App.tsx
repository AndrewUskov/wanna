import "react-native-gesture-handler";
import React from "react";
import AppNavigator from "./src/navigation/AppNavigator";
import { AppContextProvider } from "./src/context/AppContext";

import { NavigationContainer } from "@react-navigation/native";

const App: React.FC = () => {
  return (
    <AppContextProvider>
      <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
    </AppContextProvider>
  );
};

export default App;
