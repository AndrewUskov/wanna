import { useCallback } from "react";
import { Application } from "../types/applications";
import dummyApplicationData from "./../dummyData/applications.json";

export type Answers = typeof dummyApplicationData[0]["form_response"]["answers"];

export const useApplicationData = () => {
  return dummyApplicationData;
};

export const useApplicationForId = () => {
  const applicationData = useApplicationData();
  return useCallback(
    (applicationId: string) =>
      applicationData.find((item) => item.event_id === applicationId),
    []
  );
};

export const useApplicantsInfoForRestaurantId = (
  selectedRestaurantId: string
): Application[] => {
  const applicationData = useApplicantsDetailedInfoForRestaurantId(
    selectedRestaurantId
  );
  return applicationData.map((application) => ({
    id: application.event_id,
    firstName: application.form_response.answers[0]?.text,
    lastName: application.form_response.answers[1]?.text,
  }));
};

export const useApplicantsDetailedInfoForRestaurantId = (
  selectedRestaurantId: string
) => {
  const applicationData = useApplicationData();
  return applicationData.filter(
    (application) => application.restaurant.id === selectedRestaurantId
  );
};

export const useUniqueRestaurantsList = () => {
  const applicationsData = useApplicationData();

  return applicationsData
    .map((application) => application.restaurant)
    .filter(
      (restaurant, index, array) =>
        array.findIndex(
          (restaurantInner) => restaurantInner.id === restaurant.id
        ) === index
    );
};
