import { StackScreenProps } from "@react-navigation/stack";
import React, { useContext } from "react";
import { FlatList, Text, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { AppContext } from "../../context/AppContext";
import COLORS from "../../enums/Colors";
import {
  useApplicantsInfoForRestaurantId,
  useApplicationForId,
} from "../../hooks/useData";
import { RootStackParamList } from "../../navigation/RootParamList";
import { Application } from "../../types/applications";
import styles from "../Applications/styles";

type Props = StackScreenProps<RootStackParamList, "Applications">;

const Applications: React.FC<Props> = ({
  navigation,
  route: {
    params: { selectedRestaurantId },
  },
}) => {
  const { seenApplicationIds, updateSeenApplicationIds } = useContext(
    AppContext
  );
  const applicantsInfo = useApplicantsInfoForRestaurantId(selectedRestaurantId);

  const applicationForId = useApplicationForId();

  const renderItem = ({ item }: { item: Application }) => {
    const applicationHasBeenSeen = seenApplicationIds.some(
      (id) => id === item.id
    );

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          updateSeenApplicationIds(item.id);

          const application = applicationForId(item.id);

          if (!application) {
            // TODO: Add error handler
            return;
          }
          return navigation.navigate("ProfileSwiper", {
            initialApplicationId: application.id,
            selectedRestaurantId,
          });
        }}
        style={styles.wrapper}
      >
        <Text
          style={
            applicationHasBeenSeen
              ? {
                  textDecorationLine: "line-through",
                  color: COLORS.BORDER,
                }
              : undefined
          }
        >
          {item.firstName} {item.lastName}
        </Text>
        {applicationHasBeenSeen ? (
          <Text style={{ color: COLORS.GREEN }}>&#10003;</Text>
        ) : (
          <Text>{">"}</Text>
        )}
      </TouchableWithoutFeedback>
    );
  };

  const renderSeparator = () => <View style={styles.line} />;

  return (
    <View style={styles.container}>
      <FlatList
        data={applicantsInfo}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={renderSeparator}
      />
    </View>
  );
};

export default Applications;
