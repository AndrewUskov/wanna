import {StyleSheet} from "react-native";
import COLORS from "../../enums/Colors";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingBottom: 50
    },
    applicationWrapper: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    name: {
        fontSize: 24,
        fontWeight: '500',
        marginTop: 20
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginVertical: 20
    },
    field: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginVertical: 10,
        paddingHorizontal: 10
    },
    url: {
        color: COLORS.BLUE,
        textDecorationLine: 'underline'
    }
});

export default styles;
