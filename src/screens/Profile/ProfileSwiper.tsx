import { StackScreenProps } from "@react-navigation/stack";
import React, { useContext } from "react";
import { ScrollView } from "react-native";
import Swiper from "react-native-swiper";
import { AppContext } from "../../context/AppContext";
import { useApplicantsDetailedInfoForRestaurantId } from "../../hooks/useData";
import { RootStackParamList } from "../../navigation/RootParamList";
import { ProfileForm } from "./ProfileForm";
import styles from "./styles";

type Props = StackScreenProps<RootStackParamList, "ProfileSwiper">;

const ProfileSwiper: React.FC<Props> = ({
  route: {
    params: { initialApplicationId, selectedRestaurantId },
  },
}) => {
  const { updateSeenApplicationIds } = useContext(AppContext);
  const applicantsInfo = useApplicantsDetailedInfoForRestaurantId(
    selectedRestaurantId
  );

  const index = applicantsInfo.findIndex(
    (applicant) => applicant.id === initialApplicationId
  );

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.container}
    >
      <Swiper
        index={index}
        loop={false}
        showsPagination={false}
        showsButtons={false}
        onIndexChanged={(index) =>
          updateSeenApplicationIds(applicantsInfo[index].event_id)
        }
      >
        {applicantsInfo.map(({ form_response: { answers }, event_id }) => (
          <ProfileForm answers={answers} key={event_id} />
        ))}
      </Swiper>
    </ScrollView>
  );
};

export default ProfileSwiper;
