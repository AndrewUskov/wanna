import React from "react";
import { Image, Linking, Text, View } from "react-native";
import { Answers } from "../../hooks/useData";
import styles from "./styles";

interface SimpleFieldProps {
  field: string;
  value: string;
}

const SimpleField: React.FC<SimpleFieldProps> = ({ field, value }) => (
  <View style={styles.field}>
    <View style={{ flex: 1 }}>
      <Text style={{ fontWeight: "500" }}>{field}:</Text>
    </View>
    <View style={{ flex: 2 }}>
      <Text>{value}</Text>
    </View>
  </View>
);

type Props = {
  answers: Answers;
};

export const ProfileForm: React.FC<Props> = ({ answers }) => {
  return (
    <View style={styles.applicationWrapper}>
      <Text style={styles.name}>
        {answers[0].text} {answers[1].text}
      </Text>
      <Image source={{ uri: answers[13].file_url }} style={styles.image} />

      {!!answers[2]?.text && (
        <SimpleField field="Motivation" value={answers[2].text} />
      )}
      {!!answers[3]?.text && (
        <SimpleField field="Intro" value={answers[3].text} />
      )}
      {!!answers[4]?.choice?.label && (
        <SimpleField field="Position" value={answers[4].choice.label} />
      )}
      {!!answers[5]?.choice?.label && (
        <SimpleField field="Position Foh" value={answers[5].choice.label} />
      )}
      {!!answers[6]?.choice?.label && (
        <SimpleField field="Work hours" value={answers[6].choice.label} />
      )}
      {!!answers[7]?.date && (
        <SimpleField field="Start date" value={answers[7].date} />
      )}
      {!!answers[8]?.text && (
        <SimpleField field="Wine knowledge" value={answers[8].text} />
      )}
      {!!answers[9]?.choice?.label && (
        <SimpleField field="Visa type" value={answers[9].choice.label} />
      )}
      {!!answers[10]?.text && (
        <SimpleField
          field="Work experience"
          value={answers[10].text.split("\n").join("")}
        />
      )}
      {!!answers[11]?.choices?.labels && (
        <SimpleField
          field="Spoken languages"
          value={answers[11].choices.labels.join(", ")}
        />
      )}

      <View style={styles.field}>
        <View style={{ flex: 1 }}>
          <Text style={{ fontWeight: "500" }}>Url:</Text>
        </View>
        <View style={{ flex: 2 }}>
          <Text
            onPress={() =>
              answers[12].file_url && Linking.openURL(answers[12].file_url)
            }
            style={styles.url}
          >
            {answers[12].file_url}
          </Text>
        </View>
      </View>

      {!!answers[14]?.phone_number && (
        <SimpleField field="Phone number" value={answers[14].phone_number} />
      )}
      {!!answers[15]?.email && (
        <SimpleField field="Email" value={answers[15].email} />
      )}
      {!!answers[16]?.choice?.label && (
        <SimpleField
          field="Shared pool consent"
          value={answers[16].choice.label}
        />
      )}
    </View>
  );
};
