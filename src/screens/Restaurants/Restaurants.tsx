import { StackScreenProps } from "@react-navigation/stack";
import React, { useCallback } from "react";
import { FlatList, Text, View } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { useUniqueRestaurantsList } from "../../hooks/useData";
import { RootStackParamList } from "../../navigation/RootParamList";
import styles from "./styles";

type Props = StackScreenProps<RootStackParamList, "Restaurants">;

const Restaurants: React.FC<Props> = ({ navigation }) => {
  const restaurantsList = useUniqueRestaurantsList();

  const renderItem = useCallback(
    ({ item: restaurant }: { item: { id: string; label: string } }) => (
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate("Applications", {
            selectedRestaurantId: restaurant.id,
          })
        }
        style={styles.wrapper}
      >
        <Text>{restaurant.label}</Text>
        <Text>{">"}</Text>
      </TouchableWithoutFeedback>
    ),
    []
  );

  const renderSeparator = useCallback(() => <View style={styles.line} />, []);

  return (
    <View style={styles.container}>
      <FlatList
        data={restaurantsList}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={renderSeparator}
      />
    </View>
  );
};

export default Restaurants;
