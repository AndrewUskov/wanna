import {StyleSheet} from 'react-native';
import COLORS from "../../enums/Colors";

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        width: '80%',
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 15,
        alignSelf: 'center'
    },
    line: {
        width: '80%',
        height: 1,
        backgroundColor: COLORS.BORDER
    }
});

export default styles
