export type RootStackParamList = {
  Restaurants: undefined;
  Applications: { selectedRestaurantId: string };
  ProfileSwiper: { initialApplicationId: string; selectedRestaurantId: string };
};
