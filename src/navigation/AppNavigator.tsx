import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { RootStackParamList } from "./RootParamList";
import Restaurants from "../screens/Restaurants";
import Applications from "../screens/Applications";
import ProfileSwiper from "../screens/Profile";

const Stack = createStackNavigator<RootStackParamList>();

const AppNavigator: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Restaurants">
      <Stack.Screen name="Restaurants" component={Restaurants} />
      <Stack.Screen name="Applications" component={Applications} />
      <Stack.Screen name="ProfileSwiper" component={ProfileSwiper} />
    </Stack.Navigator>
  );
};

export default AppNavigator;
