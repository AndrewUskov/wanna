const COLORS = {
    BORDER: '#D9D9D9',
    BLUE: '#64C6E6',
    GREEN: '#00D6AD'
};

export default COLORS;
