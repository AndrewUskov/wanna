import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { createContext, useEffect, useState } from "react";

export type AppContextType = {
  seenApplicationIds: string[];
  updateSeenApplicationIds: (applicationId: string) => Promise<void>;
};

const seenKey = "seenApplications";

export const AppContext = createContext<AppContextType>({
  seenApplicationIds: [],
  updateSeenApplicationIds: () => {
    console.warn("updateSeenApplicationIds is not initialized");
    return Promise.resolve();
  },
});

export const AppContextProvider: React.FC = ({ children }) => {
  const [seenApplicationIds, setSeenApplicationIds] = useState<string[]>([]);

  useEffect(() => {
    getSeenApplications();
  }, []);

  const getSeenApplications = async () => {
    try {
      const data = await AsyncStorage.getItem(seenKey);
      if (data !== null) setSeenApplicationIds(JSON.parse(data));
      else setSeenApplicationIds([]);
    } catch (e) {
      console.warn(e);
    }
  };

  const updateSeenApplicationIds = async (id: string) => {
    try {
      const data = [...new Set([...seenApplicationIds, id])];
      await AsyncStorage.setItem(seenKey, JSON.stringify(data));
      setSeenApplicationIds(data);
    } catch (e) {
      console.warn(e);
    }
  };

  return (
    <AppContext.Provider
      value={{
        seenApplicationIds: seenApplicationIds,
        updateSeenApplicationIds: updateSeenApplicationIds,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
