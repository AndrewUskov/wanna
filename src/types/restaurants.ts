export type Restaurant = {
    id: string,
    label: string
}
