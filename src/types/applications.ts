export type Application = {
    id: string,
    firstName: string | undefined,
    lastName: string | undefined
}
