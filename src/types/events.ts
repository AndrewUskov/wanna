interface SimpleEventProps {
    text?: string;
    choice?: {label?: string};
    date?: string;
    choices?: {labels?: Array<string>};
    file_url?: string;
    phone_number?: string;
    email?: string
}

export type Event = {
    id: string,
    event_id: string,
    form_response: {
        answers: SimpleEventProps[]
    }
};
